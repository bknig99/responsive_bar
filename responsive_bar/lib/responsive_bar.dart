import 'dart:async';

import 'package:flutter/services.dart';

class ResponsiveBar {
  static const MethodChannel _channel = MethodChannel('responsive_bar');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
