//
//  Generated file. Do not edit.
//

import FlutterMacOS
import Foundation

import responsive_bar

func RegisterGeneratedPlugins(registry: FlutterPluginRegistry) {
  ResponsiveBarPlugin.register(with: registry.registrar(forPlugin: "ResponsiveBarPlugin"))
}
