//
//  Generated file. Do not edit.
//

// clang-format off

#include "generated_plugin_registrant.h"

#include <responsive_bar/responsive_bar_plugin.h>

void RegisterPlugins(flutter::PluginRegistry* registry) {
  ResponsiveBarPluginRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("ResponsiveBarPlugin"));
}
